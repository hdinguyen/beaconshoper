//
//  Beacon.m
//  BeaconShoper
//
//  Created by Nguyen Huynh on 3/21/15.
//  Copyright (c) 2015 nguyenh. All rights reserved.
//

#import "Beacon.h"

static NSString* const kUUID = @"";
static NSString* const kIdentifier = @"com.tedmate.iBeaconDemo";

@interface Beacon()

@property (nonatomic, strong) CLLocationManager* locationManager;
@property (nonatomic,strong) CLBeaconRegion* beaconRangingRegion;
@property (nonatomic,strong) CLBeaconRegion* beaconMonitoringRegion;

@end

@implementation Beacon

-(void) createLocationManager
{
    if (self.locationManager == nil)
    {
        self.locationManager = [[CLLocationManager alloc]init];
        [self.locationManager setDelegate:self];
    }
}

-(void)createBeaconRegionWithType:(BEACON_REGION_TYPE)type
{
    NSUUID* proximityUUID = [[NSUUID alloc]initWithUUIDString:kUUID];
    if (type == MONITORING_REGION)
    {
        if (!self.beaconMonitoringRegion)
        {
            self.beaconMonitoringRegion = [[CLBeaconRegion alloc]initWithProximityUUID:proximityUUID identifier:kIdentifier];
            [self.beaconMonitoringRegion setNotifyEntryStateOnDisplay:YES];
        }
    }
    else
    {
        if (!self.beaconRangingRegion)
        {
            self.beaconRangingRegion = [[CLBeaconRegion alloc]initWithProximityUUID:proximityUUID identifier:kIdentifier];
            [self.beaconRangingRegion setNotifyEntryStateOnDisplay:YES];
        }
    }
}

-(void)checkLocationAccess
{
    if ([self.locationManager respondsToSelector:@selector(requestAlwaysAuthorization)])
    {
        CLAuthorizationStatus authorizationStatus = [CLLocationManager authorizationStatus];
        if((authorizationStatus == kCLAuthorizationStatusDenied) || (authorizationStatus == kCLAuthorizationStatusAuthorizedWhenInUse))
        {
            UIAlertView* alert = [[UIAlertView alloc]initWithTitle:@"Setting" message:@"Required Location Access(Always) missing. Click Settings to give us access your location" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Setting", nil];
            
        }
    }
}

-(void)startRanging
{
    [self createLocationManager];
}

@end
