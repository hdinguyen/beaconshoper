//
//  Beacon.h
//  BeaconShoper
//
//  Created by Nguyen Huynh on 3/21/15.
//  Copyright (c) 2015 nguyenh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreBluetooth/CoreBluetooth.h>
#import <CoreLocation/CoreLocation.h>
#import <UIKit/UIKit.h>

typedef enum {
    RANGING_REGION = 0,
    MONITORING_REGION
} BEACON_REGION_TYPE;

@interface Beacon : NSObject <CLLocationManagerDelegate>

-(void)startRanging;
-(void)stopRanging;

-(void)startMonitoring;
-(void)stopMonitoring;
@end
