//
//  AppDelegate.h
//  BeaconShoper
//
//  Created by Nguyen Huynh on 3/21/15.
//  Copyright (c) 2015 nguyenh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ViewController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>
{
    ViewController* _viewController;
}

@property (strong, nonatomic) UIWindow *window;

@end

